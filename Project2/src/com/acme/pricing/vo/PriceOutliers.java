package com.acme.pricing.vo;

public enum PriceOutliers {
	
	PROMOTION, ERRORONOUS;
	
	public boolean isOutlier(double price, double average, double margin) {
		switch(this) {
		case PROMOTION:
			if(price < average*margin) {
				return true;
			} else {
				return false;
			}
		case ERRORONOUS:
			if(price > average*margin) {
				return true;
			} else {
				return false;
			}
		default: return false;
		}
	}

}
