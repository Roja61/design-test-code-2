package com.acme.pricing.vo;

import java.util.Map;

public class Company {
	
	private String id;
	private String name;
	private String type;
	
	private boolean sourceCompany;
	
	private Map<String,Product> products;

	public boolean isSourceCompany() {
		return sourceCompany;
	}

	public void setSourceCompany(boolean sourceCompany) {
		this.sourceCompany = sourceCompany;
	}


	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String,Product> getProducts() {
		return products;
	}

	public void setProducts(Map<String,Product> products) {
		this.products = products;
	}
	
	public Product getProduct(String id) {
		if(products != null ) {
			return products.get(id);
		}
		return null;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Company that = (Company)obj;
		return this.id.equalsIgnoreCase(that.getId());
	}

	
}
