package com.acme.pricing.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.acme.pricing.vo.Company;

public class PricingContext {
	
	private static PricingContext context = null; 
	
	private Map<String, Company> surveyData;

	private PricingContext() {
		surveyData = new HashMap<>();
	}
	
	public static PricingContext getInstance(){
		synchronized (PricingContext.class) {
			if(context == null) {
				context = new PricingContext();
			}
			return context;
		}
	}
	
	public Map<String,Company> getSurveyData() {
		return surveyData;
	}

	public void setSurveyData(Map<String,Company> surveyData) {
		this.surveyData = surveyData;
	}

}
