package com.acme.pricing.engine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.acme.pricing.vo.Demand;
import com.acme.pricing.vo.PriceOutliers;
import com.acme.pricing.vo.Product;
import com.acme.pricing.vo.Supply;

public abstract class APricingManager {
	
	private Map<String, Double> dmdSupMatrix;
	private Map<PriceOutliers, Double> outliers;
	
	public APricingManager() {
		init();
	}
	
	private void init() {
		
		dmdSupMatrix = new HashMap<>();
		dmdSupMatrix.put(Supply.H.name()+Demand.H.name(), new Double(1.0));
		dmdSupMatrix.put(Supply.L.name()+Demand.L.name(), new Double(1.10));
		dmdSupMatrix.put(Supply.L.name()+Demand.H.name(), new Double(1.05));
		dmdSupMatrix.put(Supply.H.name()+Demand.L.name(), new Double(0.95));
		
		outliers = new HashMap<>();
		
		outliers.put(PriceOutliers.PROMOTION, new Double(0.5));
		outliers.put(PriceOutliers.ERRORONOUS, new Double(1.5));
	}
	
	

	public abstract void printPricing();
	

	public Double priceVarient(Supply supply , Demand demand) {
		if(supply != null && demand != null && 
				dmdSupMatrix != null && !dmdSupMatrix.isEmpty() && 
				dmdSupMatrix.containsKey(supply.name()+demand.name())) {
			return dmdSupMatrix.get(supply.name()+demand.name());
		}
		return 0.0d;
	}
	
	public boolean isOutlier(double price, double average) {
		boolean resp = false;
		
		if(outliers  != null && !outliers.isEmpty() && price > 0 && average >0) {
			Iterator<PriceOutliers> iter = outliers.keySet().iterator();
			while(iter.hasNext()) {
				PriceOutliers key = iter.next();
				Double margin = outliers.get(key);
				if(key.isOutlier(price, average, margin)) {
					resp = true;
					break;
				}
			}
		}
		return resp;
	}
	
	public double average(Collection<Product> products) {
		double resp = 0.0d;
		if(products != null) {
			int count = 0;
			double sum = 0.0d;
			for(Product product : products) {
				sum = sum+product.getPrice();
				count++;
			}
			resp = sum/count;
		}
				
		return resp;
	}

	
	public double lowest(Collection<Product> products) {
		double lowest = 0.0d;
		if(products != null) {
			
			int count = 0;
			for(Product product : products) {
				if(count == 0) {
					lowest = product.getPrice();
				}

				if(product.getPrice() < lowest) {
					lowest = product.getPrice();
				}
				count++;
			}
		}
				
		return lowest;
	}
}
