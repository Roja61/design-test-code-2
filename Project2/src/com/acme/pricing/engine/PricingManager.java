package com.acme.pricing.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.acme.pricing.vo.Company;
import com.acme.pricing.vo.Product;

public class PricingManager extends APricingManager {

	@Override
	public void printPricing() {
		
		PricingContext context = PricingContext.getInstance();
		Map<String,Company> companies = context.getSurveyData();
		
		Iterator<String> keys = companies.keySet().iterator();
		String sourceCompany = null;
		while(keys.hasNext()) {
			String key = keys.next();
			if(companies.get(key).isSourceCompany()) {
				sourceCompany = key;
				break;
			}
		}

		if(companies.get(sourceCompany) != null && companies.get(sourceCompany).getProducts() != null &&
				companies.get(sourceCompany).getProducts().keySet() != null) {
			Iterator<String> productKeys = companies.get(sourceCompany).getProducts().keySet().iterator();
			while(productKeys.hasNext()) {
				String productId = (String)productKeys.next();
				Product sourceProduct = companies.get(sourceCompany).getProduct(productId);	
				Collection<Product> allProducts = findSimilarProducts(productId, companies);
				double average = average(allProducts);
				Collection<Product> filteredProducts = filterOutliers(allProducts, average);
				double lowestPrice = lowest(filteredProducts);
				
				Product resp = new Product();
				resp.setId(sourceProduct.getId());
				resp.setPrice(lowestPrice*priceVarient(sourceProduct.getSupply(), sourceProduct.getDemand()));
				
				System.out.println(resp);
			}
			
			
			
		}
	}
	
	private Collection<Product> findSimilarProducts(String productId, Map<String,Company> companies) {
		Collection<Product> resp = new ArrayList<>();
		
		Iterator<String> keys = companies.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			if(companies.get(key).getProduct(productId) != null) {
				resp.add(companies.get(key).getProduct(productId));
			}
		}
		
		return resp;
	}

	private Collection<Product> filterOutliers(Collection<Product> input, double average) {
		Collection<Product> resp = new ArrayList<>();
		for(Product product : input) {
			if(!isOutlier(product.getPrice(), average)) {
				resp.add(product);
			}
		}
		return resp;
	}
	
}
