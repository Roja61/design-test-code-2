package com.acme.pricing.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.acme.pricing.engine.APricingManager;
import com.acme.pricing.engine.PricingContext;
import com.acme.pricing.engine.PricingManager;
import com.acme.pricing.vo.Company;
import com.acme.pricing.vo.Demand;
import com.acme.pricing.vo.Product;
import com.acme.pricing.vo.Supply;

public class LoadData {
	
	/**
	 * 2
flashdrive H H
ssd L H
5
flashdrive X 1.0
ssd X 10.0
flashdrive Y 0.9
flashdrive Z 1.1
ssd Y 12.5

	 */

	public LoadData() {
		init();
	}

	private void init() {
		Company c1 = new Company();//c1 has 2 products namely flashdrive & ssd
		c1.setId("X");
		
		Product p1 = new Product(); 
		p1.setId("flashdrive");
		p1.setPrice(1.0d);
		p1.setDemand(Demand.H);
		p1.setSupply(Supply.H);
		

		Product p2 = new Product();
		p2.setId("ssd");
		p2.setPrice(10.0d);
		p2.setDemand(Demand.H);
		p2.setSupply(Supply.L);

		Map<String,Product> products = new HashMap<>();
		products.put(p1.getId(), p1);
		products.put(p2.getId(),p2);
		
		c1.setProducts(products);
		c1.setSourceCompany(true);
		
		
		Company c2 = new Company(); //c2 has 2 products namely flashdrive & ssd
		c2.setId("Y");
		

		Product c2p1 = new Product();
		c2p1.setId("flashdrive");
		c2p1.setPrice(0.9d);
		

		Product c2p2 = new Product();
		c2p2.setId("ssd");
		c2p2.setPrice(12.5d);

		Map<String,Product> products22 = new HashMap<>();
		products22.put(c2p1.getId(), c2p1);
		products22.put(c2p2.getId(),c2p2);
		
		c2.setProducts(products22);
		

		Company c3 = new Company(); //c3 has 1 product namely flashdrive 
		c2.setId("Z");
		

		Product p31 = new Product();
		p31.setId("flashdrive");
		p31.setPrice(1.1d);
		

		Map<String,Product> products33 = new HashMap<>();
		products33.put(p31.getId(), p31);
	
		c3.setProducts(products33);
		
		
		Map<String, Company> companies = new HashMap<>();
		companies.put(c1.getId(),c1);
		companies.put(c2.getId(),c2);
		companies.put(c3.getId(),c3);
		PricingContext context = PricingContext.getInstance();
		context.setSurveyData(companies);

	
	}
	
	public static void main(String args[]) {
		LoadData loader = new LoadData();
		APricingManager pricingMgr = new PricingManager();
		pricingMgr.printPricing();
	}
	
}
